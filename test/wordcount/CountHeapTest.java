package wordcount;

import org.junit.Before;
import org.junit.Test;
import wordcount.app.WordCount;
import wordcount.bst.BinarySearchTree;
import wordcount.data.DataCount;
import wordcount.data.DataCounter;
import wordcount.heap.CountHeap;

import java.util.ArrayList;
import static org.junit.Assert.*;

public class CountHeapTest
{
    private ArrayList<DataCount<String>> counts;
    
    @Before
    public void setUp()
    {
        String path = "texts/hamlet.txt";
        DataCounter<String> counter = new BinarySearchTree<>();
        WordCount.countWordsInFile(path, counter);
        counts = counter.getCounts();
    }

    @Test
    public void heapSort()
    {
        counts = CountHeap.sort(counts);
        
        DataCount<String> lastCount = null;
        
        for (DataCount<String> count : counts)
        {
            if (lastCount != null)
            {
                assertTrue(lastCount.compareTo(count) > 0);
            }
            lastCount = count;
        }
    }
}
