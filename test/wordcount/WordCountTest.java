package wordcount;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import wordcount.app.WordCount;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class WordCountTest
{
    private ByteArrayOutputStream capturedOutput;
    private PrintStream systemOutPrintStream;

    @Before
    public void setup()
    {
        capturedOutput = new ByteArrayOutputStream();
        systemOutPrintStream = System.out;
        System.setOut(new PrintStream(capturedOutput));
    }

    @After
    public void teardown()
    {
        System.setOut(systemOutPrintStream);
    }

    @Test
    public void binarySearchTree()
    {
        WordCount.main(new String[] {"-b", "-num_unique", "texts/hamlet.txt"});

        assertTrue(capturedOutput.toString().startsWith("4851"));
    }

    @Test
    public void avlTree()
    {
        WordCount.main(new String[] {"-a", "-num_unique", "texts/hamlet.txt"});

        assertTrue(capturedOutput.toString().startsWith("4851"));
    }

    @Test
    public void hashCounter()
    {
        WordCount.main(new String[] {"-h", "-num_unique", "texts/hamlet.txt"});

        assertTrue(capturedOutput.toString().startsWith("4851"));
    }
}