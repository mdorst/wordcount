package wordcount.bst;

/**
 * Node type for BinarySearchTree. Has a data member and it's associated
 * count, as well as left and right subtrees.
 *
 * @param <T> the type of elements being tracked in the BST
 */
class BSTNode<T extends Comparable<? super T>>
{
    /**
     * The left child of this node.
     */
    BSTNode<T> left;

    /**
     * The right child of this node.
     */
    BSTNode<T> right;

    /**
     * The data element stored at this node.
     */
    T data;

    /**
     * The count for this data element.
     */
    int count;

    /**
     * The length of the longest subtree
     */
    int height;

    /**
     * Create a new data node.
     *
     * @param data data element to be stored at this node.
     */
    BSTNode(T data) {
        this.data = data;
        count = 1;
        left = right = null;
        height = 1;
    }

    /** {@inheritDoc}
     * Helps identify nodes when debugging
     */
    @Override
    public String toString()
    {
        return "BSTNode{\"" + data + "\", " + height + "}";
    }
}
