package wordcount.bst;

/**
 * An AVL Tree, which keeps track of how many times an element is counted
 *
 * @param <E> The type of the element to be counted
 */
public class AVLTree<E extends Comparable<? super E>>
        extends BinarySearchTree<E>
{
    /**
     * Increments the count for the given element. If no such element exists
     * in the tree, one is inserted, and the count is set to 1.
     *
     * @param data the element to be counted
     */
    @Override
    public void incCount(E data)
    {
        totalCount++;
        overallRoot = insert(data, overallRoot);
    }

    /**
     * Either increases the count for the given element, or inserts it into
     * the tree if it is not already present. Balances the tree after inserting.
     * This method is recursive.
     *
     * @param data the element to be counted or inserted
     * @param node the node currently being inserted at (recursive)
     * @return either node, or the node which will replace it in the
     * tree following a balancing operation
     */
    private BSTNode<E> insert (E data, BSTNode<E> node)
    {
        if (node == null)
        {
            size++;
            return new BSTNode<>(data);
        }
        else
        {
            int cmp = data.compareTo(node.data);

            if (cmp < 0)
            {
                node.left = insert(data, node.left);
            } else if (cmp > 0)
            {
                node.right = insert(data, node.right);
            }
            else
            {
                node.count++;
            }
        }
        return balance(node);
    }

    /**
     * Performs a balancing operation on a node if needed
     *
     * @param node the node to be balanced (if needed)
     * @return either node, or the node that replaces it if a balancing
     * operation occurs
     */
    private BSTNode<E> balance(BSTNode<E> node)
    {
        if ( node == null ) return null;

        int hdiff = height(node.left) - height( node.right );

        if ( hdiff > 1 )
        {
            // Tree is left heavy
            if (height(node.left.left) >= height(node.left.right))
            {
                return singleRightRotation(node);
            }
            else
            {
                return doubleLeftRightRotation(node);
            }
        }
        else if ( hdiff < -1 )
        {
            // Tree is right heavy
            if (height(node.right.right) >= height(node.right.left))
            {
                return singleLeftRotation(node);
            }
            else
            {
                return doubleRightLeftRotation(node);
            }
        }
        // height := greater of children's heights + 1
        node.height = (height(node.left) > height(node.right))
                    ? height(node.left) + 1
                    : height(node.right) + 1;
        return node;
    }

    private int height(BSTNode<E> node)
    {
        return (node == null) ? 0 : node.height;
    }

    /**
     * Performs a single right rotation about the given node
     *
     * @param root the node to be rotated about
     * @return the node to the left of root, which is to replace root
     */
    private BSTNode<E> singleRightRotation(BSTNode<E> root)
    {
        BSTNode<E> left = root.left;
        root.left = left.right;
        left.right = root;
        root.height = Math.max(height(root.left), height(root.right)) + 1;
        left.height = Math.max(height(left.left), height(left.right)) + 1;
        return left;
    }

    /**
     * Performs a double right rotation about the given node
     *
     * @param root the node to be rotated about
     * @return the node to the right of root, which is to replace root
     */
    private BSTNode<E> doubleRightLeftRotation(BSTNode<E> root)
    {
        root.right = singleRightRotation(root.right);
        return singleLeftRotation(root);
    }

    /**
     * Performs a single left rotation about the given node
     *
     * @param root the node to be rotated about
     * @return the node to the right of root, which is to replace root
     */
    private BSTNode<E> singleLeftRotation(BSTNode<E> root)
    {
        BSTNode<E> right = root.right;
        root.right = right.left;
        right.left = root;
        root.height = Math.max(height(root.left), height(root.right)) + 1;
        right.height = Math.max(height(right.left), height(right.right)) + 1;
        return right;
    }

    /**
     * Performs a double left right rotation about the given node
     *
     * @param root the node ot be rotated about
     * @return the node to the left of root, which is to replace root
     */
    private BSTNode<E> doubleLeftRightRotation(BSTNode<E> root)
    {
        root.left = singleLeftRotation(root.left);
        return singleRightRotation(root);
    }
}
