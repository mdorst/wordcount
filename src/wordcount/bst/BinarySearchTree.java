package wordcount.bst;

import wordcount.data.DataCount;
import wordcount.data.DataCounter;

import java.util.ArrayList;

/**
 * BSTCounter implements the DataCounter interface using a binary search tree to
 * store the data items and counts.
 *
 * @param <E> The type of the data elements. Note that we have strengthened the
 *            constraints on E such that E is now a Comparable.
 */
public class BinarySearchTree<E extends Comparable<? super E>>
        implements DataCounter<E>
{

    /**
     * The root of the binary search tree. root is null if and only if the tree
     * is empty.
     */
    BSTNode<E> overallRoot;

    /**
     * Number of nodes in the binary search tree.
     */
    protected int size;
    
    /**
     * Number of times incCount has been called.
     */
    protected int totalCount;

    /**
     * Create an empty binary search tree.
     */
    public BinarySearchTree() {
        overallRoot = null;
        size = 0;
    }
    
    /** {@inheritDoc} */
    @Override
    public void incCount(E data) {
        totalCount++;
        if (overallRoot == null) {
            overallRoot = new BSTNode<>(data);
            size++;
        } else {
            // traverse the tree
            BSTNode<E> currentNode = overallRoot;
            while (true) {

                // compare the data to be inserted with the data at the current
                // node
                int cmp = data.compareTo(currentNode.data);

                if (cmp == 0) {
                    // current node is a match
                    currentNode.count++;
                    return;
                } else if (cmp < 0) {
                    // new data goes to the left of the current node
                    if (currentNode.left == null) {
                        currentNode.left = new BSTNode<>(data);
                        size++;
                        return;
                    }
                    currentNode = currentNode.left;
                } else {
                    // new data goes to the right of the current node
                    if (currentNode.right == null) {
                        currentNode.right = new BSTNode<>(data);
                        size++;
                        return;
                    }
                    currentNode = currentNode.right;
                }
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public int getUnique() {
        return size;
    }
    
    /** {@inheritDoc} */
    @Override
    public int getTotal()
    {
        return totalCount;
    }
    
    /**
     * {@inheritDoc}
     * Searches the tree for the given element, and returns the count if found,
     * or zero otherwise.
     */
    @Override
    public int getCount(E data)
    {
        BSTNode<E> n = overallRoot;
        while (n != null)
        {
            int cmp = data.compareTo(n.data);
            
            if (cmp == 0)
            {
                // Found!
                return n.count;
            }
            if (cmp < 0)
            {
                // Search left
                n = n.left;
            }
            if (cmp > 0)
            {
                // Search right
                n = n.right;
            }
        }
        // Not found!
        return 0;
    }

    /** {@inheritDoc} */
    @Override
    public ArrayList<DataCount<E>> getCounts() {

        ArrayList<DataCount<E>> counts = new ArrayList<>(size);
        for (int i = 0; i < size; i++)
        {
            counts.add(null);
        }
        if (overallRoot != null)
            traverse(overallRoot, counts, 0);
        return counts;
    }

    /**
     * Do an inorder traversal of the tree, filling in an array of DataCount
     * objects with the count of each element. Doing an inorder traversal
     * guarantees that the result will be sorted by element. We fill in some
     * contiguous block of array elements, starting at index, and return the
     * next available index in the array.
     *
     * @param counts The array to populate.
     */
    private int traverse(BSTNode<E> root, ArrayList<DataCount<E>> counts,
                         int idx)
    {
        if(root != null)
        {
            idx = traverse(root.left, counts, idx);
            counts.set(idx, new DataCount<>(root.data, root.count));
            idx = traverse(root.right, counts, idx + 1);
        }
        return idx;
    }
}
