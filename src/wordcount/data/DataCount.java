package wordcount.data;
/**
 * Simple class to hold a piece of data and its count. The class has package
 * access so that the various implementations of DataCounter can
 * access its
 * contents, but not client code.
 *
 * @param <E> type of data whose count we are recording.
 */
public class DataCount<E extends Comparable<? super E>>
        implements Comparable<DataCount<E>>
{
    /**
     * The data element whose count we are recording.
     */
    public E data;
    
    /**
     * The count for the data element.
     */
    public int count;
    
    /**
     * Create a new data count.
     *
     * @param data the data element whose count we are recording.
     * @param count the count for the data element.
     */
    public DataCount(E data, int count) {
        this.data = data;
        this.count = count;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(DataCount<E> dc)
    {
        if (count != dc.count)
        {
            return count - dc.count;
        }
        else
        {
            return dc.data.compareTo(data);
        }
    }
}
