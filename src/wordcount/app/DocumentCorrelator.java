package wordcount.app;

import wordcount.bst.AVLTree;
import wordcount.bst.BinarySearchTree;
import wordcount.data.DataCount;
import wordcount.data.DataCounter;
import wordcount.hash.HashCounter;

import java.util.ArrayList;

/**
 * An executable that counts the words in two files and prints a difference
 * metric which corresponds to the square of the Euclidean distance between the
 * two vectors in the space of shared words in the documents. Note that this
 * metric assumes that words not present in both documents do not affect the
 * correlation.
 */
public class DocumentCorrelator
{
    public static void main(String[] args)
    {
        if (args.length != 3)
        {
            invalidArguments();
            System.exit(1);
        }
        boolean bst = args[0].equals("-b");
        boolean avl = args[0].equals("-a");
        boolean hash = args[0].equals("-h");
        String path1 = args[1];
        String path2 = args[2];
        DataCounter<String> counter1 = null;
        DataCounter<String> counter2 = null;
        
        if (bst)
        {
            counter1 = new BinarySearchTree<>();
            counter2 = new BinarySearchTree<>();
        }
        if (avl)
        {
            counter1 = new AVLTree<>();
            counter2 = new AVLTree<>();
        }
        if (hash)
        {
            counter1 = new HashCounter();
            counter2 = new HashCounter();
        }
        if (counter1 == null)
        {
            invalidArguments();
            System.exit(1);
        }
        
        WordCount.countWordsInFile(path1, counter1);
        WordCount.countWordsInFile(path2, counter2);
    
        int totalCounts1 = counter1.getTotal();
        int totalCounts2 = counter2.getTotal();
        ArrayList<DataCount<String>> wordCounts1 = counter1.getCounts();
        
        // The sum of the squares of the differences of the normalized
        // frequencies of each word within a frequency threshold.
        double differenceMetric = 0.0;
        
        for (DataCount<String> wordData1 : wordCounts1)
        {
            double frequency1 = wordData1.count / (double) totalCounts1;
            // Ignore words with frequencies above 1% and below 0.01%
            if (frequency1 > 0.0001 && frequency1 < 0.01)
            {
                int wordCount2 = counter2.getCount(wordData1.data);
                double frequency2 = wordCount2 / (double) totalCounts2;
                
                if (frequency2 > 0.0001 && frequency2 < 0.01)
                {
                    double difference = frequency1 - frequency2;
                    // Add the square of the difference in frequencies
                    differenceMetric += difference * difference;
                }
            }
        }
        System.out.printf("Difference metric: %.8f", differenceMetric);
    }
    
    private static void invalidArguments()
    {
        System.err.println("Usage: [ -b | -a | -h ] <filename1> <filename2>");
    }
}
