package wordcount.app;

import wordcount.bst.AVLTree;
import wordcount.bst.BinarySearchTree;
import wordcount.data.DataCount;
import wordcount.data.DataCounter;
import wordcount.hash.HashCounter;
import wordcount.heap.CountHeap;
import wordcount.util.FileWordReader;

import java.io.IOException;
import java.util.ArrayList;

/**
 * An executable that counts the words in a file and prints either the frequency
 * of each word, or the number of unique words, depending on the command line
 * arguments
 */
public class WordCount
{
    public static void main(String[] args)
    {
        if (args.length < 3)
        {
            invalidArguments();
            System.exit(1);
        }
        boolean bst = args[0].equals("-b");
        boolean avl = args[0].equals("-a");
        boolean hash = args[0].equals("-h");
        boolean freq = args[1].equals("-frequency");
        boolean uniq = args[1].equals("-num_unique");
        String path = args[2];

        DataCounter<String> counter = null;

        if (!freq && !uniq)
        {
            invalidArguments();
            System.exit(1);
        }

        if (bst)
        {
            counter = new BinarySearchTree<>();
        }
        if (avl)
        {
            counter = new AVLTree<>();
        }
        if (hash)
        {
            counter = new HashCounter();
        }
        if (counter == null)
        {
            invalidArguments();
            System.exit(1);
        }

        countWordsInFile(path, counter);

        if (freq)
        {
            ArrayList<DataCount<String>> counts = counter.getCounts();

            counts = CountHeap.sort(counts);

            for (DataCount<String> wc : counts)
            {
                System.out.println(wc.count + " " + wc.data);
            }
        }
        if (uniq)
        {
            System.out.println(counter.getUnique());
        }
    }

    /**
     * Print usage instructions and quit
     */
    private static void invalidArguments()
    {
        System.err.println("Usage: [ -b | -a | -h] [ -frequency | " +
                                   "-num_unique] <filename>");
    }

    /**
     * Read a file and count the words used in it using the given counter
     * @param path the path of the file to count the words in
     * @param counter the counter which will track the word counts
     */
    public static void countWordsInFile(String path,
                                        DataCounter<String> counter)
    {
        try
        {
            FileWordReader reader = new FileWordReader(path);
            String word = reader.nextWord();

            while (word != null)
            {
                counter.incCount(word);

                word = reader.nextWord();
            }
        }
        catch (IOException e)
        {
            System.err.println("Error processing " + path + e);
            System.exit(1);
        }
    }
}
