package wordcount.hash;

/**
 * Cell tracks a single word and it's associated count.
 * It also has a reference to the next cell in the bucket, allowing cells to be
 * nodes in a linked list.
 */
class Cell
{
    String word;
    int count = 1;
    Cell next = null;
    
    Cell(String w)
    {
        word = w;
    }
}
