package wordcount.hash;

import java.util.Iterator;

/**
 * Maintains a linked list of Cells
 */
class Bucket implements Iterable<Cell>
{
    private Cell head;

    /*
     * Adds a new `Cell` in front of `head` and increments `length`
     */
    void add(String word)
    {
        if (head == null)
        {
            head = new Cell(word);
        } else
        {
            Cell cell = new Cell(word);
            cell.next = head;
            head = cell;
        }
    }

    /** {@inheritDoc} */
    @Override
    public Iterator<Cell> iterator()
    {
        return new BucketIterator(head);
    }
}
