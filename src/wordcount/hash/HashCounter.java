package wordcount.hash;

import wordcount.data.DataCount;
import wordcount.data.DataCounter;

import java.util.ArrayList;
import java.util.List;

/**
 * HashCounter implements the DataCounter interface using a hash table to
 * store the data items and counts.
 */
public class HashCounter implements DataCounter<String>
{
    /**
     * The number of buckets in the table
     */
    private static final int K = 10000;
    
    /**
     * The number of elements in the table
      */
    private int n = 0;
    
    /**
     * The number of times incCount has been called
     */
    private int totalCounts;
    
    private List<Bucket> table;
    
    public HashCounter()
    {
        table = new ArrayList<>(K);
        
        for (int i = 0; i < K; i++)
        {
            table.add(i, new Bucket());
        }
    }

    /** {@inheritDoc} */
    @Override
    public void incCount(String word)
    {
        totalCounts++;
        int hashCode = hash(word);
        
        Bucket bucket = table.get(hashCode % K);
        
        for (Cell c : bucket)
        {
            if (c.word.equals(word))
            {
                // Found
                c.count++;
                return;
            }
        }
        // Not found
        bucket.add(word);
        n++;
    }

    /** {@inheritDoc} */
    @Override
    public int getUnique()
    {
        return n;
    }
    
    /** {@inheritDoc} */
    @Override
    public int getTotal()
    {
        return totalCounts;
    }

    /** {@inheritDoc} */
    @Override
    public ArrayList<DataCount<String>> getCounts()
    {
        ArrayList<DataCount<String>> counts = new ArrayList<>(n);

        for (Bucket bucket : table)
        {
            for (Cell cell : bucket)
            {
                counts.add(new DataCount<>(cell.word, cell.count));
            }
        }
        return counts;
    }
    
    /**
     * {@inheritDoc}
     * Searches by doing a hash find
     */
    @Override
    public int getCount(String word)
    {
        int hash = hash(word);
        
        Bucket bucket = table.get(hash % K);
        
        for (Cell cell : bucket)
        {
            if (cell.word.equals(word))
            {
                // Found!
                return cell.count;
            }
        }
        // Not found!
        return 0;
    }

    /**
     * Hash function for strings
     * @param key the string to be hashed
     * @return the hash value
     */
    private int hash(String key)
    {
        int h = 0;
        for (int i = 0; i < key.length(); i++)
        {
            h++;
            h *= key.charAt(i);
        }
        return Math.abs(h);
    }
}
