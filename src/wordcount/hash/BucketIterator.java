package wordcount.hash;

import java.util.Iterator;

public class BucketIterator implements Iterator<Cell>
{
    private Cell cell;
    
    BucketIterator(Cell c)
    {
        cell = c;
    }
    
    @Override
    public boolean hasNext()
    {
        return cell != null;
    }
    
    @Override
    public Cell next()
    {
        if (cell == null) return null;
        
        Cell temp = cell;
        cell = cell.next;
        return temp;
    }
}
