package wordcount.heap;

import wordcount.data.DataCount;

import java.util.ArrayList;
import java.util.List;

/**
 * Maintains a heap of {@code DataCount<E>}, ordered by count in descending
 * order
 *
 * @param <E> The type of data being counted
 */
public class CountHeap<E extends Comparable<? super E>>
{
    private List<DataCount<E>> array;
    
    /**
     * Constructs a heap from an unsorted array in O(n) time
     *
     * @param a the array to be built into a heap
     */
    private CountHeap(ArrayList<DataCount<E>> a)
    {
        array = new ArrayList<>(a.size() + 1);
        // array[0] is unused - reserve it so the first element is at array[1]
        array.add(null);
        array.addAll(a);

        // Let i = the largest even index
        // Get the largest index
        int i = array.size() - 1;
        // If i is odd, decrement i
        if (i % 2 != 0) i--;

        // Iterate backward by twos through the array
        for (; i > 1 ; i -= 2)
        {
            // n will be the index of a node which will start at i, and then
            // percolates down as long as n has a child that is greater than n
            int n = i;
            // while n has a child
            for (int m; n < array.size(); n = m * 2)
            {
                // Let leftVal = the data at index n
                // Let rightVal = the data at index n+1 (or null if n+1 is past
                // the end of array)
                // Let parentVal = the data at n/2
                DataCount<E> leftVal = array.get(n);
                DataCount<E> rightVal = (n+1 == array.size())
                                     ? null : array.get(n+1);
                DataCount<E> parentVal = array.get(n/2);

                // Let m = the index of the greater of leftVal and rightVal
                // Let maxChildVal = the data at index m
                m = (rightVal == null || leftVal.compareTo(rightVal) > 0)
                        ? (n) : (n + 1);
                DataCount<E> maxChildVal = array.get(m);

                // If maxChildVal is greater than parentVal, swap their values
                if (maxChildVal.compareTo(parentVal) > 0)
                {
                    array.set(n/2, maxChildVal);
                    array.set(m, parentVal);
                }
                else
                {
                    // If no swap occurred, there is no need to percolate down
                    break;
                }
            }
        }
    }

    /**
     * Removes and returns the element with the highest count
     *
     * @return the element with the highest count
     */
    private DataCount<E> delete()
    {
        if (array.size() < 2) return null;
        
        DataCount<E> highestCount = array.get(1);
        // Let lastLeaf = the rightmost element in the bottom row of the tree
        DataCount<E> lastLeaf = array.remove(array.size() - 1);
        
        int pos = 1, nextPos;
        
        // while pos is not a leaf node
        while (pos <= (array.size() - 1) / 2)
        {
            // nextPos = the greater of pos' children
            if (pos * 2 + 1 >= array.size() ||
                array.get(pos * 2).compareTo(array.get(pos * 2 + 1)) > 0)
            {
                // nextPos = the left child
                nextPos = pos * 2;
            }
            else
            {
                // nextPos = the right child
                nextPos = pos * 2 + 1;
            }

            // if nextPos > lastLeaf, nextPos should bubble up to pos
            if (array.get(nextPos).compareTo(lastLeaf) > 0)
            {
                array.set(pos, array.get(nextPos));
            }
            else
            {
                break;
            }
            // if nextPos bubbled up, repeat this process for nextPos
            pos = nextPos;
        }
        // Either nextPos was not bubbled up, or we have reached a leaf node.
        // pos is the most recently bubbled up node. We store lastLeaf at pos.
        if (array.size() > 1) array.set(pos, lastLeaf);
        return highestCount;
    }
    
    /**
     * Sorts an array of {@code DataCount<T>} by building a heap and then repeatedly
     * deleting from the heap.
     *
     * @param <T> The type of data being counted
     * @param a the array to be sorted
     */
    public static <T extends Comparable<? super T>>
    ArrayList<DataCount<T>> sort(ArrayList<DataCount<T>> a)
    {
        CountHeap<T> heap = new CountHeap<>(a);
        ArrayList<DataCount<T>> r = new ArrayList<>(a.size());
        
        for (DataCount<T> dc : a)
        {
            r.add(heap.delete());
        }
        return r;
    }
}
